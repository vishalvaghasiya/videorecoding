//
//  ViewController.swift
//  VideoRecoding
//
//  Created by KMSOFT on 03/03/17.
//  Copyright © 2017 KMSOFT. All rights reserved.
//

import UIKit
import AVKit
import MobileCoreServices
import AVFoundation
class ViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,AVAudioPlayerDelegate,AVAudioRecorderDelegate{

    var audioPlayer: AVAudioPlayer?
    var audioRecorder: AVAudioRecorder?
    
    @IBAction func VideoRecording(_ sender: Any) {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera){
            print("Camera Is Available")
            let ImagePicker = UIImagePickerController()
            
            ImagePicker.delegate = self
            ImagePicker.sourceType = .camera
            ImagePicker.allowsEditing = false
            ImagePicker.mediaTypes = [kUTTypeMovie as String]
            
            ImagePicker.showsCameraControls = true
            self.present(ImagePicker, animated: true, completion: nil)
            
        }else{
            print("Camera Not Unavailable")
        }
        
    }
    @IBAction func AudioRecording(_ sender: Any) {
        print("Button Click")
        if audioRecorder?.isRecording == false {
            PlayButton.isEnabled = false
            StopButton.isEnabled = true
            audioRecorder?.record()
            print("Start Recording")
        }
    }
    
    @IBAction func AudioPlay(_ sender: Any) {
     
        if audioRecorder?.isRecording == false {
            PlayButton.setImage(UIImage(named: "Pause.png"), for: UIControlState())
            StopButton.isEnabled = true
            RecordButton.isEnabled = false
            do {
                try audioPlayer = AVAudioPlayer(contentsOf:
                    (audioRecorder?.url)!)
                audioPlayer!.delegate = self
                audioPlayer!.prepareToPlay()
                audioPlayer!.play()
            } catch let error as NSError {
                print("audioPlayer error: \(error.localizedDescription)")
            }
        }
    }
    @IBAction func AudioStop(_ sender: Any) {
        StopButton.isEnabled = false
        PlayButton.isEnabled = true
        RecordButton.isEnabled = true
        
        if audioRecorder?.isRecording == true {
            audioRecorder?.stop()
        } else {
            PlayButton.setImage(UIImage(named: "Play.png"), for: UIControlState())
            audioPlayer?.stop()
        }
    }
    
    @IBOutlet weak var PlayButton: UIButton!
    @IBOutlet weak var StopButton: UIButton!
    @IBOutlet weak var RecordButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        PlayButton.isEnabled = false
        StopButton.isEnabled = false
        
         let fileMgr = FileManager.default
        let dirPaths = fileMgr.urls(for: .documentDirectory,
                                    in: .userDomainMask)
        
        let soundFileURL = dirPaths[0].appendingPathComponent("sound.caf")
        
        let recordSettings =
            [AVEncoderAudioQualityKey: AVAudioQuality.min.rawValue,
             AVEncoderBitRateKey: 16,
             AVNumberOfChannelsKey: 2,
             AVSampleRateKey: 44100.0] as [String : Any]
        
        let audioSession = AVAudioSession.sharedInstance()
        
        do {
            try audioSession.setCategory(
                AVAudioSessionCategoryPlayAndRecord)
        } catch let error as NSError {
            print("audioSession error: \(error.localizedDescription)")
        }
        
        do {
            try audioRecorder = AVAudioRecorder(url: soundFileURL,
                                                settings: recordSettings as [String : AnyObject])
            audioRecorder?.prepareToRecord()
        } catch let error as NSError {
            print("audioSession error: \(error.localizedDescription)")
        }
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Delegate Method
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        RecordButton.isEnabled = true
        StopButton.isEnabled = false
        PlayButton.setImage(UIImage(named: "Play.png"), for: UIControlState())
    }
    
    func audioPlayerDecodeErrorDidOccur(_ player: AVAudioPlayer, error: Error?) {
        print("Audio Play Decode Error")
    }
    
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
    }
    
    func audioRecorderEncodeErrorDidOccur(_ recorder: AVAudioRecorder, error: Error?) {
        print("Audio Record Encode Error")
    }
    
    // save Video In My File Manager
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let mediaType = info[UIImagePickerControllerMediaType] as! NSString
        dismiss(animated: true, completion: nil)
        // Handle a movie capture
        if mediaType == kUTTypeMovie {
            guard let path = (info[UIImagePickerControllerMediaURL] as! NSURL).path else { return }
            if UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(path) {

                UISaveVideoAtPathToSavedPhotosAlbum(path, self, #selector(ViewController.video(videoPath:didFinishSavingWithError:contextInfo:)), nil)
            }
        }
    }
    
    func video(videoPath: NSString, didFinishSavingWithError error: NSError?, contextInfo info: AnyObject) {
        var title = "Success"
        var message = "Video was saved"
        if let _ = error {
            title = "Error"
            message = "Video failed to save"
        }
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    // End Video Save In my File Manager

}

